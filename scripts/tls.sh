mkdir -p ./tls/

step certificate create \
    --profile root-ca \
    --insecure \
    --no-password \
    ca tls/ca.crt.pem tls/ca.key.pem

SAN="$(printf '--san 172.28.28.%s ' $(seq $((CONSUL_REPLICA_COUNT + 2)) $((CONSUL_REPLICA_COUNT + PATRONI_REPLICA_COUNT + PGBOUNCER_REPLICA_COUNT + 1))))"

step certificate create \
    --insecure \
    --no-password \
    --ca ./tls/ca.crt.pem \
    --ca-key ./tls/ca.key.pem \
    --san patroni \
    --san consul \
    --san pgbouncer \
    --san localhost \
    --san 127.0.0.1 \
    $(echo $SAN) \
    server ./tls/server.crt.pem ./tls/server.key.pem

step certificate create \
    --insecure \
    --no-password \
    --ca ./tls/ca.crt.pem \
    --ca-key ./tls/ca.key.pem \
    --san localhost \
    --san 127.0.0.1 \
    $(echo $SAN) \
    client ./tls/client.crt.pem ./tls/client.key.pem

chmod 755 ./tls
chmod 644 ./tls/*.pem
