#!/usr/bin/env bash

source scripts/helpers.sh

export TEST_IMAGE="${TEST_IMAGE:-registry.gitlab.com/gitlab-org/omnibus-gitlab/gitlab-ee}"
export TEST_TAG="${TEST_TAG:-master}"

export TEST_APP_CFG="${TEST_APP_CFG:-app.rb}"
export TEST_CONSUL_CFG="${TEST_CONSUL_CFG:-consul.rb}"
export TEST_PATRONI_CFG="${TEST_PATRONI_CFG:-patroni.rb}"
export TEST_PGBOUNCER_CFG="${TEST_PGBOUNCER_CFG:-pgbouncer.rb}"

export DOCKER_COMPOSE_FILE="docker-compose${COMPOSE_FILE_SUFFIX}.yaml"

export CONSUL_REPLICA_COUNT=3
export PATRONI_REPLICA_COUNT=3
export PGBOUNCER_REPLICA_COUNT=1

GITLAB_SQL_USER='gitlab'
GITLAB_SQL_PASSWORD='secret'
PGBOUNCER_SQL_USER='pgbouncer'
PGBOUNCER_SQL_PASSWORD='secret'
REPLICATOR_SQL_USER='gitlab_replicator'
REPLICATOR_SQL_PASSWORD='secret'
CONSUL_USER='gitlab-consul'
CONSUL_PASSWORD='secret'

tee .env <<EOF
GITLAB_DOMAIN='http://gitlab.test'
GITLAB_SQL_USER="${GITLAB_SQL_USER}"
GITLAB_SQL_PASSWORD="${GITLAB_SQL_PASSWORD}"
GITLAB_SQL_PASSWORD_HASH="$(pg_password_md5 ${GITLAB_SQL_USER} ${GITLAB_SQL_PASSWORD})"

PGBOUNCER_SQL_USER="${GITLAB_SQL_USER}"
PGBOUNCER_SQL_PASSWORD="${PGBOUNCER_SQL_PASSWORD}"
PGBOUNCER_SQL_PASSWORD_HASH="$(pg_password_md5 ${PGBOUNCER_SQL_USER} ${PGBOUNCER_SQL_PASSWORD})"

REPLICATOR_SQL_USER="${REPLICATOR_SQL_USER}"
REPLICATOR_SQL_PASSWORD="${REPLICATOR_SQL_PASSWORD}"
REPLICATOR_SQL_PASSWORD_HASH="$(pg_password_md5 ${REPLICATOR_SQL_USER} ${REPLICATOR_SQL_PASSWORD})"

CONSUL_USER="${CONSUL_USER}"
CONSUL_PASSWORD="${CONSUL_PASSWORD}"
CONSUL_PASSWORD_HASH="$(pg_password_md5 ${CONSUL_USER} ${CONSUL_PASSWORD})"

CONSUL_REPLICA_COUNT=${CONSUL_REPLICA_COUNT}
PATRONI_REPLICA_COUNT=${PATRONI_REPLICA_COUNT}
PGBOUNCER_REPLICA_COUNT=${PGBOUNCER_REPLICA_COUNT}
EOF

[ "${USE_TLS}" = 'true' ] && source scripts/tls.sh

compose() {
  docker-compose \
    -f ${DOCKER_COMPOSE_FILE} \
    -p gitlab \
    ${@}
}

setup() {
  compose up -d \
  --scale patroni=${PATRONI_REPLICA_COUNT} \
  --scale consul=${CONSUL_REPLICA_COUNT} \
  --scale pgbouncer=${PGBOUNCER_REPLICA_COUNT} \
  --scale patroni_new=0
}

spawn_new_leader() {
  compose up -d \
  --scale patroni=${PATRONI_REPLICA_COUNT} \
  --scale consul=${CONSUL_REPLICA_COUNT} \
  --scale pgbouncer=${PGBOUNCER_REPLICA_COUNT} \
  --scale patroni_new=1
}

grow_new_cluster() {
  compose up -d \
  --scale patroni=${PATRONI_REPLICA_COUNT} \
  --scale consul=${CONSUL_REPLICA_COUNT} \
  --scale pgbouncer=${PGBOUNCER_REPLICA_COUNT} \
  --scale patroni_new=${PATRONI_REPLICA_COUNT}
}

teardown_old_cluster() {
  compose up -d \
  --scale patroni=0 \
  --scale consul=${CONSUL_REPLICA_COUNT} \
  --scale pgbouncer=${PGBOUNCER_REPLICA_COUNT} \
  --scale patroni_new=${PATRONI_REPLICA_COUNT}
}

teardown() {
  compose down
}

configure_pgb() {
  compose exec pgbouncer \
    gitlab-ctl write-pgpass --host 127.0.0.1 --database pgbouncer --user pgbouncer --hostuser gitlab-consul

  ## Enter password for `pgbouncer` user
}

run_migrations() {
  compose exec app \
    gitlab-rake gitlab:db:configure
}

logs() {
  compose logs -f ${@}
}

enter() {
  local service_name="${1%/*}"
  local service_index="${1#*/}"
  [ "${service_index}" = "${service_name}" ] && service_index=1

  shift

  compose exec --index="${service_index}" "${service_name}" ${@:-bash}
}

gitlab_ctl() {
  local service_ref="${1}"; shift

  enter "${service_ref}" gitlab-ctl ${@}
}

reconfigure() {
  local service_name="${1%/*}"
  local service_index="${1#*/}"
  [ "${service_index}" = "${service_name}" ] && service_index=1

  shift

  compose exec --index="${service_index}" "${service_name}" \
    gitlab-ctl reconfigure
}
