roles ['pgbouncer_role']

pgbouncer['admin_users'] = %w(pgbouncer gitlab-consul)
pgbouncer['users'] = {
  'gitlab-consul': {
    password: ENV['CONSUL_PASSWORD_HASH']
  },
  'pgbouncer': {
    password: ENV['PGBOUNCER_SQL_PASSWORD_HASH']
  }
}

consul['watchers'] = %w(postgresql)
consul['configuration'] = {
  retry_join: %w(consul)
}

## upgrade ##

# consul['watchers'] = %w(postgresql_new)
# consul['internal']['postgresql_service_name'] = 'postgresql_new'

### disable unused services ###

gitlab_rails['auto_migrate'] = false
gitlab_exporter['enable'] = false
sidekiq['enable'] = false
puma['enable'] = false
registry['enable'] = false
gitaly['enable'] = false
gitlab_workhorse['enable'] = false
nginx['enable'] = false
prometheus_monitoring['enable'] = false
redis['enable'] = false
gitlab_kas['enable'] = false
