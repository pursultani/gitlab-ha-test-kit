roles ['patroni_role']

postgresql['listen_address'] = '0.0.0.0'
postgresql['pgbouncer_user_password'] = ENV['PGBOUNCER_SQL_PASSWORD_HASH']
postgresql['sql_user_password'] = ENV['GITLAB_SQL_PASSWORD_HASH']
postgresql['sql_replication_password'] = ENV['REPLICATOR_SQL_PASSWORD']
postgresql['trust_auth_cidr_addresses'] = %w(127.0.0.1/32 172.28.28.0/24)
postgresql['md5_auth_cidr_addresses'] = %w(127.0.0.1/32 172.28.28.0/24)

consul['services'] = %w(postgresql)
consul['configuration'] = {
  retry_join: %w(consul),
  ca_file: '/opt/tls/ca.crt.pem',
  cert_file: '/opt/tls/client.crt.pem',
  key_file: '/opt/tls/client.key.pem',
  verify_incoming: true,
  verify_outgoing: true,
  ports: {
    http: -1,
    https: 8501
  }
}

patroni['consul']['url'] = 'https://consul:8501'
patroni['consul']['cacert'] = '/opt/tls/ca.crt.pem'
patroni['consul']['cert'] = '/opt/tls/client.crt.pem'
patroni['consul']['key'] = '/opt/tls/client.key.pem'
patroni['consul']['verify'] = true

patroni['tls_ca_file'] = '/opt/tls/ca.crt.pem'
patroni['tls_certificate_file'] = '/opt/tls/server.crt.pem'
patroni['tls_key_file'] = '/opt/tls/server.key.pem'
patroni['tls_client_mode'] = 'required'
patroni['tls_client_certificate_file'] = '/opt/tls/client.crt.pem'
patroni['tls_client_key_file'] = '/opt/tls/client.key.pem'
patroni['tls_verify'] = true


### disable unused services ###

gitlab_rails['auto_migrate'] = false
gitlab_exporter['enable'] = false
sidekiq['enable'] = false
puma['enable'] = false
registry['enable'] = false
gitaly['enable'] = false
gitlab_workhorse['enable'] = false
nginx['enable'] = false
prometheus_monitoring['enable'] = false
redis['enable'] = false
gitlab_kas['enable'] = false
