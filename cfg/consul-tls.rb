roles ['consul_role']

consul['use_tls'] = true
consul['tls_ca_file'] = '/opt/tls/ca.crt.pem'
consul['tls_certificate_file'] = '/opt/tls/server.crt.pem'
consul['tls_key_file'] = '/opt/tls/server.key.pem'
consul['tls_verify_client'] = nil

consul['configuration'] = {
  server: true,
  client_addr: '0.0.0.0',
  bootstrap_expect: ENV['CONSUL_REPLICA_COUNT'].to_i,
  retry_join: %w(consul),
  ports: {
    http: -1,
    https: 8501
  }
}

### disable unused services ###

gitlab_exporter['enable'] = false
sidekiq['enable'] = false
puma['enable'] = false
registry['enable'] = false
gitaly['enable'] = false
gitlab_workhorse['enable'] = false
nginx['enable'] = false
prometheus_monitoring['enable'] = false
redis['enable'] = false
gitlab_rails['auto_migrate'] = false
gitlab_kas['enable'] = false
