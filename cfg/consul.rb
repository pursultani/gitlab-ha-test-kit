consul['enable'] = true

consul['configuration'] = {
  server: true,
  client_addr: '0.0.0.0',
  bootstrap_expect: ENV['CONSUL_REPLICA_COUNT'].to_i,
  retry_join: %w(consul)
}

### disable unused services ###

gitlab_exporter['enable'] = false
sidekiq['enable'] = false
puma['enable'] = false
registry['enable'] = false
gitaly['enable'] = false
gitlab_workhorse['enable'] = false
nginx['enable'] = false
prometheus_monitoring['enable'] = false
redis['enable'] = false
gitlab_rails['auto_migrate'] = false
gitlab_kas['enable'] = false
