external_url ENV['GITLAB_DOMAIN']

gitlab_rails['db_host'] = 'pgbouncer'
gitlab_rails['db_port'] = 6432
gitlab_rails['db_password'] = ENV['GITLAB_SQL_PASSWORD']

# run database migrations manually
gitlab_rails['auto_migrate'] = false

### disable unused services ###

postgresql['enable'] = false
gitlab_exporter['enable'] = false
prometheus_monitoring['enable'] = false
grafana['enable'] = false
gitlab_kas['enable'] = false
