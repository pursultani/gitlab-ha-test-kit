roles ['patroni_role']

postgresql['listen_address'] = '0.0.0.0'
postgresql['pgbouncer_user_password'] = ENV['PGBOUNCER_SQL_PASSWORD_HASH']
postgresql['sql_user_password'] = ENV['GITLAB_SQL_PASSWORD_HASH']
postgresql['sql_replication_password'] = ENV['REPLICATOR_SQL_PASSWORD']
postgresql['trust_auth_cidr_addresses'] = %w(127.0.0.1/32 172.28.28.0/24)
postgresql['md5_auth_cidr_addresses'] = %w(127.0.0.1/32 172.28.28.0/24)

consul['services'] = %w(postgresql)
consul['configuration'] = {
  retry_join: %w(consul)
}

patroni['consul']['url'] = 'http://consul:8500'

## upgrade ##

# patroni['postgresql']['wal_level'] = 'logical'

### disable unused services ###

gitlab_rails['auto_migrate'] = false
gitlab_exporter['enable'] = false
sidekiq['enable'] = false
puma['enable'] = false
registry['enable'] = false
gitaly['enable'] = false
gitlab_workhorse['enable'] = false
nginx['enable'] = false
prometheus_monitoring['enable'] = false
redis['enable'] = false
gitlab_kas['enable'] = false
