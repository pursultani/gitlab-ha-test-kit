roles ['pgbouncer_role']

pgbouncer['admin_users'] = %w(pgbouncer gitlab-consul)
pgbouncer['users'] = {
  'gitlab-consul': {
    password: ENV['CONSUL_PASSWORD_HASH']
  },
  'pgbouncer': {
    password: ENV['PGBOUNCER_SQL_PASSWORD_HASH']
  }
}

consul['watchers'] = %w(postgresql)
consul['configuration'] = {
  retry_join: %w(consul),
  ca_file: '/opt/tls/ca.crt.pem',
  cert_file: '/opt/tls/client.crt.pem',
  key_file: '/opt/tls/client.key.pem',
  verify_incoming: true,
  verify_outgoing: true,
  ports: {
    http: -1,
    https: 8501
  }
}


### disable unused services ###

gitlab_rails['auto_migrate'] = false
gitlab_exporter['enable'] = false
sidekiq['enable'] = false
puma['enable'] = false
registry['enable'] = false
gitaly['enable'] = false
gitlab_workhorse['enable'] = false
nginx['enable'] = false
prometheus_monitoring['enable'] = false
redis['enable'] = false
gitlab_kas['enable'] = false
